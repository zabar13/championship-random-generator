import {
  setGeneratedTeams,
  setResults,
  resetTeamsReducer,
  setGroups
} from "./teamsActionCreators";
import teamsGenerator from "../utils/teamsGenerator";
import groupStageTableGenerator from "../utils/groupStageTableGenerator";
import groupsGenerator from "../utils/groupsGenerator";
import { resetPlayoffReducer } from "./playoffActionCreators";

export function generateTeams() {
  return dispatch => {
    const teams = teamsGenerator(16);
    dispatch(resetTeamsReducer());
    dispatch(resetPlayoffReducer());
    dispatch(setGeneratedTeams(teams));
  };
}

export function drawGroups(teams) {
  return dispatch => {
    const groups = groupsGenerator(teams);
    dispatch(setGroups(groups));
  };
}

export function generateResults(teams, name) {
  return dispatch => {
    const { matchResults, table } = groupStageTableGenerator(teams);
    let completeResults = {
      [name]: { matchResults, table }
    };
    dispatch(setResults(completeResults));
  };
}
