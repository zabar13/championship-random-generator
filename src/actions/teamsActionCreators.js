import { ACTIONS } from "../reducers/teamsReducer";

export function setGeneratedTeams(teams) {
  return {
    type: ACTIONS.SET_TEAMS,
    teams
  };
}

export function setGroups(groups) {
  return {
    type: ACTIONS.SET_GROUPS,
    groups
  };
}

export function resetTeamsReducer() {
  return {
    type: ACTIONS.RESET_ALL
  };
}

export function setResults(results) {
  return {
    type: ACTIONS.SET_RESULTS,
    results
  };
}

export function setGroupTab(groupTab) {
  return {
    type: ACTIONS.SET_GROUP_TAB,
    groupTab
  };
}
