import _ from "lodash";

import {
  setQuaterfinalsPairs,
  setQuaterfinalsResults,
  setSemifinalsResults,
  setFinalsResults
} from "./playoffActionCreators";
import { drawGroup } from "../utils/groupsGenerator";
import playoffMatchDataGenerator from "../utils/playoffMatchDataGenerator";
import pickQualificatedTeams from "../utils/pickQualificatedTeams";

export function drawQuaterfinalsPair(results, allTeams) {
  return dispatch => {
    const winners = pickQualificatedTeams(results, 1);
    const runnerups = pickQualificatedTeams(results, 2);
    const drawnWinners = _.flatten(
      drawGroup(winners).map(winner => {
        return allTeams.filter(team => {
          return team.name == winner.team;
        });
      })
    );
    const drawnRunnerups = _.flatten(
      drawGroup(runnerups).map(winner => {
        return allTeams.filter(team => {
          return team.name == winner.team;
        });
      })
    );

    let pairs = [];
    for (let i = 0; i < drawnWinners.length; i++) {
      pairs = [...pairs, [drawnWinners[i], drawnRunnerups[i]]];
    }

    dispatch(setQuaterfinalsPairs(pairs));
  };
}

export function generateQuaterfinalsResults(teams) {
  return dispatch => {
    const { results, winners } = playoffMatchDataGenerator(teams);
    const semifinalPairs = [[winners[0], winners[1]], [winners[2], winners[3]]];

    dispatch(setQuaterfinalsResults(results, semifinalPairs));
  };
}

export function generateSemifinalsResults(teams) {
  return dispatch => {
    const { results, winners } = playoffMatchDataGenerator(teams);
    const finalists = [[winners[0], winners[1]]];
    dispatch(setSemifinalsResults(results, finalists));
  };
}

export function generateFinalResults(teams) {
  return dispatch => {
    const { results, winners } = playoffMatchDataGenerator(teams);

    dispatch(setFinalsResults(results, winners));
  };
}
