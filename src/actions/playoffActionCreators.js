import { ACTIONS } from '../reducers/playoffReducer';

export function setQuaterfinalsPairs(quaterfinalsPairs) {
    return {
        type: ACTIONS.SET_QUARETFINALS,
        quaterfinalsPairs
    };
}

export function setQuaterfinalsResults(quaterfinalsResults, semifinalsPairs) {
    return {
        type: ACTIONS.SET_QUARETFINALSRESULTS,
        quaterfinalsResults,
        semifinalsPairs
    };
}

export function setSemifinalsResults(semifinalsResults, finalists) {
    return {
        type: ACTIONS.SET_SEMIFINALSRESULTS,
        semifinalsResults,
        finalists
    };
}

export function setFinalsResults(finalResults, champion) {
    return {
        type: ACTIONS.SET_FINALRESULTS,
        finalResults,
        champion
    };
}

export function resetPlayoffReducer() {
    return {
        type: ACTIONS.RESET
    };
}