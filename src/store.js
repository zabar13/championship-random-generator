import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import teamsReducer from './reducers/teamsReducer';
import playoffReducer from './reducers/playoffReducer';

const store = createStore(combineReducers({
    teamsReducer,
    playoffReducer
}),applyMiddleware(thunk));

export default store;