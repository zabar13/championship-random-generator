import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import '@babel/polyfill/noConflict';

import './css/main.css';
import App from './components/App';
import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <MemoryRouter>
            <App />
        </MemoryRouter>
    </Provider>,
    document.getElementById('root')
)
