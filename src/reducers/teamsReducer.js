export const ACTIONS = {
  SET_TEAMS: "SET_TEAMS",
  SET_RESULTS: "SET_RESULTS",
  RESET_ALL: "RESET_ALL",
  SET_GROUPS: "SET_GROUPS",
  SET_GROUP_TAB: "SET_GROUP_TAB"
};

const DEFAULT_STATE = {
  teams: [],
  results: {},
  groups: {
    groupA: [],
    groupB: [],
    groupC: [],
    groupD: []
  },
  groupTab: "A"
};

export default function teamsReducer(
  state = DEFAULT_STATE,
  { type, teams, results, groups, groupTab }
) {
  switch (type) {
    case ACTIONS.SET_TEAMS:
      return {
        ...state,
        teams: [...teams]
      };
    case ACTIONS.SET_RESULTS:
      return {
        ...state,
        results: {
          ...state.results,
          ...results
        }
      };
    case ACTIONS.SET_GROUPS:
      return {
        ...state,
        groups
      };
    case ACTIONS.SET_GROUP_TAB:
      return {
        ...state,
        groupTab
      };
    case ACTIONS.RESET_ALL:
      return {
        ...DEFAULT_STATE
      };
    default:
      return state;
  }
}
