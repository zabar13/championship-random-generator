export const ACTIONS = {
    SET_QUARETFINALS: 'SET_QUARETFINALS',
    SET_QUARETFINALSRESULTS: 'SET_QUARETFINALSRESULTS',
    SET_SEMIFINALSRESULTS: 'SET_SEMIFINALSRESULTS',
    SET_FINALRESULTS: 'SET_FINALRESULTS',
    RESET: 'RESET'
}

const DEFAULT_STATE = {
    quaterfinalsPairs: []
}

export default function playoffReducer(state = DEFAULT_STATE,
    { type, quaterfinalsPairs, quaterfinalsResults, semifinalsPairs,
        semifinalsResults, finalists, finalResults, champion }) {
    switch (type) {
        case ACTIONS.SET_QUARETFINALS:
            return {
                ...state,
                quaterfinalsPairs
            };
        case ACTIONS.SET_QUARETFINALSRESULTS:
            return {
                ...state,
                quaterfinalsResults,
                semifinalsPairs
            };
        case ACTIONS.SET_SEMIFINALSRESULTS:
            return {
                ...state,
                semifinalsResults,
                finalists
            };
        case ACTIONS.SET_FINALRESULTS:
            return {
                ...state,
                finalResults,
                champion
            };
        case ACTIONS.RESET:
            return DEFAULT_STATE;
        default:
            return state;
    };
}