import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Header from './Header';
import DrawStage from './drawStage/DrawStage';
import Left from './Left';
import GroupStage from './groupStage/GroupStage';
import PlayOffStage from './playOffStage/PlayOffStage';

export default function App() {
    return (
        <div>
            <Header />
            <Left />
            <Switch>
                <Route
                    path="/"
                    exact
                    component={DrawStage} />
                <Route
                    path="/groupStage"
                    exact
                    component={GroupStage} />
                <Route
                    path="/playoff"
                    exact
                    component={PlayOffStage} />
            </Switch>
        </div>
    );
}