import React from 'react';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';


export default function Left() {
    return (
        <div className='left-menu'>
            <NavLink to='/' exact
                style={{ textDecoration: 'none' }}
                activeClassName='is-active'>
                <Button
                    variant='contained'
                    color='primary'>
                    HOME
                    </Button>
            </NavLink>
            <NavLink to='/groupStage'
                style={{ textDecoration: 'none' }}
                activeClassName='is-active'>
                <Button
                    variant='contained'
                    color='primary'>
                    GROUP STAGE
                    </Button>
            </NavLink>
            <NavLink to='/playoff'
                style={{ textDecoration: 'none' }}
                activeClassName='is-active'>
                <Button
                    variant='contained'
                    color='primary'>
                    PLAY-OFF STAGE
                    </Button>
            </NavLink>
        </div >
    );
};