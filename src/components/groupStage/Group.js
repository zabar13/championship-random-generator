import React from "react";
import { Paper, Button, Tooltip } from "@material-ui/core";

export default function Group({
  name,
  teams,
  groupResults = { matchResults: [], table: [] },
  generateResults,
  value,
  index
}) {
  const { matchResults, table } = groupResults;

  const tooltipText = `Generate results and calculate table for ${name}.`;
  return (
    <div
      className={`group-container ${
        value !== index ? "group-stage-hidden" : ""
      }`}
    >
      <div>
        <Tooltip title={tooltipText} placement="top">
          <div>
            <Button
              disabled={teams.length === 0 || table.length !== 0}
              variant="contained"
              color="primary"
              onClick={() => generateResults()}
            >
              {`Generate results ${name}`}
            </Button>
          </div>
        </Tooltip>
        <Paper className="teams-paper">TEAMS</Paper>
        {teams.map((team, i) => {
          return (
            <Paper key={i} className="teams-paper">
              {team.name}
            </Paper>
          );
        })}
      </div>
      <div className="group-data-container">
        <Paper className="teams-paper">RESULTS</Paper>
        {matchResults.map((result, i) => {
          return (
            <div className="result-container" key={`${result.homeTeam}${i}`}>
              <Paper className="group-teamName">{result.homeTeam}</Paper>
              <Paper className="group-score">{result.homeTeamGoals}</Paper>
              <Paper className="group-score">{result.awayTeamGoals}</Paper>
              <Paper className="group-teamName">{result.awayTeam}</Paper>
            </div>
          );
        })}
      </div>
      <div className="group-data-container">
        <Paper className="teams-paper">TABLE</Paper>
        {table.map((team, i) => {
          return (
            <Paper key={i} className="teams-paper results">
              <table>
                <tbody>
                  <tr>
                    <td>{team.team}</td>
                    <td>{team.points}</td>
                    <td>{team.goalsScored}</td>
                    <td>{team.goalsLost}</td>
                    <td>{team.goalsDifference}</td>
                  </tr>
                </tbody>
              </table>
            </Paper>
          );
        })}
      </div>
    </div>
  );
}
