import React from "react";
import { connect } from "react-redux";
import { Tabs, Tab } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

import Group from "./Group";
import { generateResults } from "../../actions/teamsActions";
import { setGroupTab } from "../../actions/teamsActionCreators";

export function GroupStage({
  groups,
  results,
  groupTab,
  setGroupTab,
  generateResults
}) {
  const { groupA, groupB, groupC, groupD } = groups;

  const handleChange = (event, groupTab) => {
    setGroupTab(groupTab);
  };

  return (
    <div className="main">
      <Tabs
        value={groupTab}
        indicatorColor="primary"
        textColor="primary"
        onChange={handleChange}
        variant="fullWidth"
        centered
      >
        <Tab className="group-tab" value="A" label="Group A" />
        <Tab className="group-tab" value="B" label="Group B" />
        <Tab className="group-tab" value="C" label="Group C" />
        <Tab className="group-tab" value="D" label="Group D" />
      </Tabs>
      <div className="groups-container">
        <Group
          value={groupTab}
          index="A"
          name="GROUP A"
          teams={groupA}
          groupResults={results.groupA}
          generateResults={() => generateResults(groupA, "groupA")}
        />
        <Group
          value={groupTab}
          index="B"
          name="GROUP B"
          teams={groupB}
          groupResults={results.groupB}
          generateResults={() => generateResults(groupB, "groupB")}
        />
        <Group
          value={groupTab}
          index="C"
          name="GROUP C"
          teams={groupC}
          groupResults={results.groupC}
          generateResults={() => generateResults(groupC, "groupC")}
        />
        <Group
          value={groupTab}
          index="D"
          name="GROUP D"
          teams={groupD}
          groupResults={results.groupD}
          generateResults={() => generateResults(groupD, "groupD")}
        />
      </div>
    </div>
  );
}

export default connect(
  state => {
    const { groups, results, groupTab } = state.teamsReducer;
    return { groups, results, groupTab };
  },
  { setGroupTab, generateResults }
)(GroupStage);
