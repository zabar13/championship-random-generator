import React from "react";
import { connect } from "react-redux";
import { Button, Paper, Tooltip } from "@material-ui/core";

import { generateFinalResults } from "../../actions/playoffActions";

export function Final({
  finalists = [],
  generateFinalResults,
  finalResults = [],
  champion = []
}) {
  return (
    <div className="playoff-segment-container">
      <div className="playoff-segment-title">
        <div>FINAL</div>
      </div>
      <div className="playoff-btn-container">
        <Tooltip title="Generate final results.">
          <div>
            <Button
              disabled={finalists.length == 0 || finalResults.length != 0}
              variant="contained"
              color="primary"
              onClick={() => generateFinalResults(finalists)}
            >
              RESULTS
            </Button>
          </div>
        </Tooltip>
      </div>
      <div>
        {" "}
        {finalResults.length > 0
          ? finalResults.map((result, i) => {
              return (
                <div className="result-container" key={i}>
                  <Paper className="group-teamName">{result.homeTeam}</Paper>
                  <Paper className="group-score">{result.homeTeamGoals}</Paper>
                  <Paper className="group-score">{result.awayTeamGoals}</Paper>
                  <Paper className="group-teamName">{result.awayTeam}</Paper>
                </div>
              );
            })
          : finalists.map((pair, i) => {
              return (
                <div className="result-container" key={i}>
                  <Paper className="group-teamName">{pair[0].name}</Paper>
                  <Paper className="group-score">{}</Paper>
                  <Paper className="group-score">{}</Paper>
                  <Paper className="group-teamName">{pair[1].name}</Paper>
                </div>
              );
            })}
      </div>
      {champion.map((team, i) => {
        return (
          <div key={i} className="playoff-segment-container">
            <div className="playoff-segment-title">
              <div>CHAMPION</div>
            </div>
            <Paper className="teams-paper">
              <h3>{team.name}</h3>
            </Paper>
          </div>
        );
      })}
    </div>
  );
}
export default connect(
  state => {
    const { finalists, finalResults, champion } = state.playoffReducer;
    return {
      finalists,
      finalResults,
      champion
    };
  },
  { generateFinalResults }
)(Final);
