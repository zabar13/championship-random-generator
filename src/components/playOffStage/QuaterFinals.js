import React from "react";
import { connect } from "react-redux";
import { Button, Paper, Tooltip } from "@material-ui/core";

import {
  drawQuaterfinalsPair,
  generateQuaterfinalsResults
} from "../../actions/playoffActions";

export function QuaterFinals({
  results,
  allTeams,
  drawQuaterfinalsPair,
  quaterfinalsPairs,
  generateQuaterfinalsResults,
  quaterfinalsResults = []
}) {
  return (
    <div className="playoff-segment-container">
      <div className="playoff-segment-title">
        <div>QUATERFINALS</div>
      </div>
      <div>
        <div className="playoff-btn-container">
          <Tooltip title="Draw quaterfilnals pair.">
            <div>
              <Button
                disabled={
                  quaterfinalsPairs.length > 0 ||
                  Object.keys(results).length < 4
                }
                variant="contained"
                color="primary"
                onClick={() => drawQuaterfinalsPair(results, allTeams)}
              >
                DRAW
              </Button>
            </div>
          </Tooltip>
          <Tooltip title="Generate quaterfilnals results.">
            <div>
              <Button
                disabled={
                  quaterfinalsPairs.length == 0 ||
                  quaterfinalsResults.length != 0
                }
                variant="contained"
                color="primary"
                onClick={() => generateQuaterfinalsResults(quaterfinalsPairs)}
              >
                RESULTS
              </Button>
            </div>
          </Tooltip>
        </div>
        <div className="playoff-segment-container">
          {" "}
          {quaterfinalsResults.length > 0
            ? quaterfinalsResults.map((result, i) => {
                return (
                  <div className="result-container" key={i}>
                    <Paper className="group-teamName">{result.homeTeam}</Paper>
                    <Paper className="group-score">
                      {result.homeTeamGoals}
                    </Paper>
                    <Paper className="group-score">
                      {result.awayTeamGoals}
                    </Paper>
                    <Paper className="group-teamName">{result.awayTeam}</Paper>
                  </div>
                );
              })
            : quaterfinalsPairs.map((pair, i) => {
                return (
                  <div className="result-container" key={i}>
                    <Paper className="group-teamName">{pair[0].name}</Paper>
                    <Paper className="group-score">{}</Paper>
                    <Paper className="group-score">{}</Paper>
                    <Paper className="group-teamName">{pair[1].name}</Paper>
                  </div>
                );
              })}
        </div>
      </div>
    </div>
  );
}

export default connect(
  state => {
    const { results, teams: allTeams } = state.teamsReducer;
    const { quaterfinalsPairs, quaterfinalsResults } = state.playoffReducer;
    return {
      allTeams,
      results,
      quaterfinalsPairs,
      quaterfinalsResults
    };
  },
  { drawQuaterfinalsPair, generateQuaterfinalsResults }
)(QuaterFinals);
