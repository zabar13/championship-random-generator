import React from 'react';
import QuaterFinals from './QuaterFinals';
import SemiFinals from './SemiFinals';
import Final from './Final';

export default function PlayOffSTage() {
    return (
        <div className="main">
            <QuaterFinals/>
            <SemiFinals/>
            <Final/>
        </div>
    );
}
