import React from "react";
import { connect } from "react-redux";
import { Button, Paper, Tooltip } from "@material-ui/core";

import { generateSemifinalsResults } from "../../actions/playoffActions";

export function SemiFinals({
  generateSemifinalsResults,
  semifinalsPairs = [],
  semifinalsResults = []
}) {
  return (
    <div className="playoff-segment-container">
      <div className="playoff-segment-title">
        <div>SEMIFINALS</div>
      </div>
      <div>
        <div className="playoff-btn-container">
          <Tooltip title="Generate Semifinals results.">
            <div>
              <Button
                disabled={
                  semifinalsPairs.length == 0 || semifinalsResults.length != 0
                }
                variant="contained"
                color="primary"
                onClick={() => generateSemifinalsResults(semifinalsPairs)}
              >
                RESULTS
              </Button>
            </div>
          </Tooltip>
        </div>
        <div className="playoff-segment-container">
          {semifinalsResults.length > 0
            ? semifinalsResults.map((result, i) => {
                return (
                  <div className="result-container" key={i}>
                    <Paper className="group-teamName">{result.homeTeam}</Paper>
                    <Paper className="group-score">
                      {result.homeTeamGoals}
                    </Paper>
                    <Paper className="group-score">
                      {result.awayTeamGoals}
                    </Paper>
                    <Paper className="group-teamName">{result.awayTeam}</Paper>
                  </div>
                );
              })
            : semifinalsPairs.map((pair, i) => {
                return (
                  <div className="result-container" key={i}>
                    <Paper className="group-teamName">{pair[0].name}</Paper>
                    <Paper className="group-score">{}</Paper>
                    <Paper className="group-score">{}</Paper>
                    <Paper className="group-teamName">{pair[1].name}</Paper>
                  </div>
                );
              })}
        </div>
      </div>
    </div>
  );
}

export default connect(
  state => {
    const { semifinalsPairs, semifinalsResults } = state.playoffReducer;
    return {
      semifinalsPairs,
      semifinalsResults
    };
  },
  { generateSemifinalsResults }
)(SemiFinals);
