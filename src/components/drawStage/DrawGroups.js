import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';

import { drawGroups } from '../../actions/teamsActions';
import Group from './Group';



export function DrawGroups({ drawGroups, teams, groups }) {
    const { groupA, groupB, groupC, groupD } = groups;

    return (
        <div className="draw-groups">
            <div>
                <Tooltip title="Draw generated teams to four groups">
                    <div>
                    <Button
                        disabled={teams.length == 0 || groupA.length != 0}
                        variant="contained"
                        color="primary"
                        onClick={() => drawGroups(teams)}>
                        DRAW GROUPS
                    </Button>
                    </div>
                </Tooltip>
            </div>
            <div className="generate-teams-container">
                <Group name='GROUP A' teams={groupA} />
                <Group name='GROUP B' teams={groupB} />
                <Group name='GROUP C' teams={groupC} />
                <Group name='GROUP D' teams={groupD} />
            </div>
        </div>
    )
}

export default connect(
    state => {
        const { teams, groups } = state.teamsReducer;
        return { teams, groups }
    },
    { drawGroups }
)(DrawGroups)