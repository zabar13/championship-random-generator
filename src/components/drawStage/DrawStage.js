import React from 'react';

import  GenerateTeams  from './GenerateTeams';
import DrawGroups from './DrawGroups';

export default function DrawStage() {
return (
        <div className="main">
            <GenerateTeams />
            <DrawGroups />
        </div>
    );
};