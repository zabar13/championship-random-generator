import React from "react";
import Paper from "@material-ui/core/Paper";

export default function Group({ name, teams }) {
  return (
    <div className="group">
      {teams.length > 0 ? (
        <div>
          <Paper className="teams-paper">
            <h4>{name}</h4>
          </Paper>
        </div>
      ) : (
        <div></div>
      )}
      <div>
        {teams.map((team, i) => {
          return (
            <Paper key={i} className="teams-paper">
              {team.name}
            </Paper>
          );
        })}
      </div>
    </div>
  );
}
