import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";

import { generateTeams } from "../../actions/teamsActions";

export function GenerateTeams({ teams, generateTeams }) {
  return (
    <div className="generate-teams">
      <Tooltip title="Generate new teams">
        <Button variant="contained" color="primary" onClick={generateTeams}>
          GENERATE TEAMS
        </Button>
      </Tooltip>
      <div className="generate-teams-container">
        {teams.map((team, i) => {
          return (
            <div className="team-container" key={i}>
              <Paper className="teams-paper">{team.name}</Paper>
              {team.attack ? (
                <Paper className=" stats-paper">{`Attack : ${team.attack} Defense : ${team.defense}`}</Paper>
              ) : (
                <Paper className="stats-paper"></Paper>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
}
export default connect(
  state => {
    const { teams } = state.teamsReducer;
    return {
      teams: teams.length > 0 ? teams : Array(16).fill({ name: "EMPTY" })
    };
  },
  { generateTeams }
)(GenerateTeams);
