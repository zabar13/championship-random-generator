export const cities = [
    'Warsaw', 'Poznan',
    'Berlin', 'Munich',
    'Torino', 'Roma',
    'Madrit', 'Barcelona',
    'Paris', 'Monaco',
    'Porto', 'Benfica',
    'Praha', 'Liberec',
    'Salzburg', 'Wien',
    'Zagreb', 'Split',
    'London', 'Liverpool',
    'Moscow', 'Kazan',
    'Goteborg', 'Malmo'
];
export const customNames = [
    'Strikers', 'Warriors', 'Fighters', 'Devils',
    'Phoenixes', 'Raptors', 'Predators',
    'Goliaths', 'Titans', 'Giants', 'Dwarfs',
    'Lions', 'Tigers', 'Panthers', 'Hawks',
    'Hunters', 'Prowlers', 'Masters', 'Rovers',
    'United', 'City', 'County', 'Athletic',
    'Blues', 'Reds', 'Greens', 'Blacks',
    'FC', 'UTD', 'SV'
];