import _ from 'lodash';

export default function groupsGenerator(teams) {
    const noOfGroups = 4;
    let availableTeams = [...teams];
    let groups = [];

    for (let i = 0; i < noOfGroups; i++) {
        groups.push(drawGroup(availableTeams));
    };

    return {
        groupA: groups[0],
        groupB: groups[1],
        groupC: groups[2],
        groupD: groups[3]
    };
}

export function drawGroup(availableTeams) {
    let group = [];
    for (let i = 0; i < 4; i++) {
        const drawnNo = Math.floor(Math.random() * availableTeams.length)
        const drawnTeam = availableTeams.splice(drawnNo, 1);
        group = [...group, drawnTeam];
    };
    return _.flatten(group);
}