import matchResultGenerator from "./matchResultGenerator";

export default function groupMatchDataGenerator(homeTeam, awayTeam) {
  const { homeTeamGoals, awayTeamGoals } = matchResultGenerator(
    homeTeam,
    awayTeam
  );
  const { homeTeamPoints, awayTeamPoints } = points(
    homeTeamGoals,
    awayTeamGoals
  );

  return {
    results: {
      homeTeam: homeTeam.name,
      homeTeamGoals,
      awayTeamGoals,
      awayTeam: awayTeam.name
    },
    [homeTeam.name]: {
      points: homeTeamPoints,
      goalsScored: homeTeamGoals,
      goalsLost: awayTeamGoals
    },
    [awayTeam.name]: {
      points: awayTeamPoints,
      goalsScored: awayTeamGoals,
      goalsLost: homeTeamGoals
    }
  };
}

function points(homeGoals, awayGoals) {
  switch (true) {
    case homeGoals === awayGoals:
      return {
        homeTeamPoints: 1,
        awayTeamPoints: 1
      };
    case homeGoals > awayGoals:
      return {
        homeTeamPoints: 3,
        awayTeamPoints: 0
      };
    case homeGoals < awayGoals:
      return {
        homeTeamPoints: 0,
        awayTeamPoints: 3
      };
    default:
      return;
  }
}
