import _ from "lodash";

import groupMatchDataGenerator from "./groupMatchDataGenerator";

export default function groupStageTableGenerator(teams) {
  const allGroupResults = generateGroupStageResults(teams);

  const matchResults = allGroupResults.map(result => result.results);

  const table = makeTable(allGroupResults, teams);

  return { matchResults, table };
}

function generateGroupStageResults(teams) {
  const [teamA, teamB, teamC, teamD] = [...teams];
  let allGroupResults = [];

  const groupMatches = [
    [teamA, teamB],
    [teamC, teamD],
    [teamA, teamC],
    [teamB, teamD],
    [teamA, teamD],
    [teamB, teamC]
  ];

  groupMatches.forEach(match => {
    allGroupResults.push(groupMatchDataGenerator(match[0], match[1]));
  });

  return allGroupResults;
}

function makeTable(results, teams) {
  const table = teams.map(team => {
    const teamResults = results.filter(item => item.hasOwnProperty(team.name));
    const result = calculateTeamData(team.name, teamResults);
    return result;
  });

  return _.sortBy(table, [
    "points",
    "goalsScored",
    "goalsDifference"
  ]).reverse();
}

function calculateTeamData(team, teamResults) {
  let points = teamResults
    .map(result => result[team].points)
    .reduce((a, b) => {
      return a + b;
    }, 0);
  let goalsScored = teamResults
    .map(result => result[team].goalsScored)
    .reduce((a, b) => {
      return a + b;
    }, 0);
  let goalsLost = teamResults
    .map(result => result[team].goalsLost)
    .reduce((a, b) => {
      return a + b;
    }, 0);

  const teamTable = {
    team,
    points,
    goalsScored,
    goalsLost,
    goalsDifference: goalsScored - goalsLost
  };

  return teamTable;
}
