import dice from './dice';

export default function matchResultGenerator(homeTeam, awayTeam) {
    const homeTeamGoals = goalsCalc(homeTeam, awayTeam);
    const awayTeamGoals = goalsCalc(awayTeam, homeTeam);

    return { homeTeamGoals, awayTeamGoals };
}


function goalsCalc(teamOne, teamTwo) {
    return (teamOne.attack - teamTwo.defense) > 0
        ? Math.ceil((teamOne.attack - teamTwo.defense) / 2) + dice(4)
        : Math.floor(dice(4) / 2);
}