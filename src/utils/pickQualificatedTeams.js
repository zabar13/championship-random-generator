export default function pickQualificatedTeams(results, place) {
    const groups = Object.keys(results);

    return groups.map(group => {
        return results[group].table[place-1];
    });
}
