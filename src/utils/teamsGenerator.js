import { cities, customNames } from "../const";
import dice from "./dice";

export default function teamsGenerator(noOfTeams) {
  let teams = [];
  for (let i = 0; i < noOfTeams; i++) {
    const generatedTeam = teamGenerator();
    if (teams.map(team => team.name).indexOf(generatedTeam.name) === -1) {
      teams.push(generatedTeam);
    } else {
      noOfTeams++;
    }
  }
  return teams;
}

function teamGenerator() {
  const name = randomTeamNameGenarator(cities, customNames);
  const attack = dice(10);
  const defense = dice(10);

  return {
    name,
    attack,
    defense
  };
}

function randomTeamNameGenarator(cities, customNames) {
  const city = cities[Math.ceil(Math.random() * cities.length - 1)];
  const name = customNames[Math.ceil(Math.random() * customNames.length - 1)];

  return `${city} ${name}`;
}
