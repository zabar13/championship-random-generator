import matchResultGenerator from "./matchResultGenerator";
import dice from "./dice";

export default function playoffMatchDataGenerator(matchs) {
  let results = [];
  let winners = [];
  matchs.map(match => {
    let { homeTeamGoals, awayTeamGoals } = matchResultGenerator(
      match[0],
      match[1]
    );
    let winner = "";
    if (homeTeamGoals !== awayTeamGoals) {
      winner = match[checkWinner(homeTeamGoals, awayTeamGoals)];
    } else {
      let drawnWinner = dice(2);
      drawnWinner == 1 ? homeTeamGoals++ : awayTeamGoals++;
      winner = match[checkWinner(homeTeamGoals, awayTeamGoals)];
    }
    winners = [...winners, winner];
    results = [
      ...results,
      {
        homeTeam: match[0].name,
        homeTeamGoals,
        awayTeamGoals,
        awayTeam: match[1].name
      }
    ];
  });

  return {
    results,
    winners
  };
}

function checkWinner(homeTeamGoals, awayTeamGoals) {
  if (homeTeamGoals > awayTeamGoals) {
    return 0;
  } else {
    return 1;
  }
}
